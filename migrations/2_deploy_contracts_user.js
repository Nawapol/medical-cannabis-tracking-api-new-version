
const SuperAdminContract = artifacts.require('SuperAdminContract.sol')
const UserContract = artifacts.require('UserContract.sol')
const FarmContract = artifacts.require('FarmContract.sol')
const TransportContract = artifacts.require('TransportContract.sol')
const FactoryContract = artifacts.require('FactoryContract.sol')
const AgencyContract = artifacts.require('AgencyContract.sol')
const DoPTokenContract = artifacts.require('DoseProductToken.sol')
const RaPTokenContract = artifacts.require('RawProductToken.sol')

module.exports = function(deployer, network, accounts){
  return deployer.deploy(DoPTokenContract).then(() => {
    return deployer.deploy(RaPTokenContract).then(() => {
      return deployer.deploy(UserContract,DoPTokenContract.address,RaPTokenContract.address).then(() => {
        return deployer.deploy(FarmContract,DoPTokenContract.address,RaPTokenContract.address).then(() => {
          return deployer.deploy(TransportContract,DoPTokenContract.address,RaPTokenContract.address).then(() => {
            return deployer.deploy(FactoryContract,DoPTokenContract.address,RaPTokenContract.address).then(() => {
              return deployer.deploy(AgencyContract,DoPTokenContract.address,RaPTokenContract.address).then(() => {
                return deployer.deploy(SuperAdminContract,UserContract.address,FarmContract.address,TransportContract.address,FactoryContract.address,AgencyContract.address,DoPTokenContract.address,RaPTokenContract.address).then(() => {
                  // console.log('SuperAdminContract address is : ' + SuperAdminContract.address);
                  // console.log('UserContract address is : ' + UserContract.address);
                  // console.log('FarmContract address is : ' + FarmContract.address);
                  // console.log('TransportContract address is : ' + TransportContract.address);
                  // console.log('FactoryContract address is : ' + FactoryContract.address);
                  // console.log('AgencyContract address is : ' + AgencyContract.address);
                });    
              })
            })
          })
        })
      })
    })
  })
}

