const express = require('express')
const bodyParser = require('body-parser')
const config = require('./config')
var http = require('http')
var uniqid = require('uniqid')
const moment = require('moment')
const fs = require('fs')
var cors = require('cors')
var multer = require('multer')

// config

var port = process.env.PORT || 8080;
var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}))

app.listen(port,function() {
    console.log('Start node.js on port ' + port)
})

app.use(express.static(__dirname + '/public'))

// api

app.get('/',(req, res) => {
    res.send('hello' + SuperAdminContract);
})

app.get('/cheatSuperAdmin',(req, res) => {
    const key = uniqid('USER-');
    var object = {
        'address': config.super_address,
        'key': key
    }
    console.log(object);
    res.send(object);
})