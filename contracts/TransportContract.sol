pragma solidity ^0.4.24;
pragma experimental ABIEncoderV2;

contract TransportContract {
    
    mapping (address => bool) private _owners;
    
    function OwnedContract(address addr) public returns(bool){
        _owners[addr] = true;
        return true;
    }
    
    struct Transport{
        bytes32 transport_key;
        address builder_address;
        string transport_location;
        bytes32 latlng;
        bytes32 transport_status;
        bytes32 license;
        string transport_detail;
    }
    
    struct Car{
        bytes32 car_key;
        bytes32 transport_key;
        address car_address;
        bytes32 datetime;
        bytes32 status;
        string car_detail;
        string transport_detail;
    }
    
    event Transports(
        bytes32 transport_key,
        address builder_address,
        string transport_location,
        bytes32 latlng,
        bytes32 transport_status,
        bytes32 license,
        string transport_detail
    );
    
    event Cars(
        bytes32 car_key,
        bytes32 transport_key,
        address car_address,
        bytes32 datetime,
        bytes32 status,
        string car_detail,
        string transport_detail
    );
    
    
    mapping (bytes32 => Transport) transports;
    mapping (bytes32 => Transport[]) transport_update;
    mapping (bytes32 => Car) cars;
    mapping (bytes32 => Car[]) car_update;
    
    function createTransport(string _transport_key, address _builder_address, string _transport_location, string _latlng, string _transport_status, 
    string _license, string _transport_detail)public returns(bool){
        
        bytes32 transport_key = stringToBytes32(_transport_key);
        bytes32 latlng = stringToBytes32(_latlng);
        bytes32 transport_status = stringToBytes32(_transport_status);
        bytes32 license = stringToBytes32(_license);
        
        transports[transport_key] = Transport(transport_key, _builder_address, _transport_location, latlng, transport_status, license, _transport_detail);
        transport_update[transport_key].push(Transport(transport_key,_builder_address, _transport_location, latlng, transport_status, license, _transport_detail));
        return true;
        
    }
    
    function createCar(string _car_key, string _transport_key, address _car_address, string _datetime, string _status, string _car_detail, string _transport_detail)
    public returns(bool) {
        
        bytes32 car_key = stringToBytes32(_car_key);
        bytes32 transport_key = stringToBytes32(_transport_key);
        bytes32 datetime = stringToBytes32(_datetime);
        bytes32 status = stringToBytes32(_status);
        
        cars[car_key] = Car(car_key, transport_key, _car_address, datetime, status, _car_detail, _transport_detail);
        car_update[car_key].push(Car(car_key, transport_key, _car_address, datetime, status, _car_detail, _transport_detail));
        return true;
        
    }
    
    function getTransport(string _transport_key)public returns(Transport) {
        
        bytes32 transport_key = stringToBytes32(_transport_key);
        
        Transport use = transports[transport_key];
        emit Transports(use.transport_key, use.builder_address, use.transport_location, use.latlng, use.transport_status, use.license, use.transport_detail);
        return use;
    }
    
    function getCar(string _car_key)public returns(Car) {
        
        bytes32 car_key = stringToBytes32(_car_key);
        
        Car use = cars[car_key];
        emit Cars(use.car_key,use.transport_key,use.car_address,use.datetime,use.status,use.car_detail,use.transport_detail);
        return use;
    }
    
    function getTransportHistory(string _transport_key)public returns(Transport[]){
        
        bytes32 transport_key = stringToBytes32(_transport_key);
        
        Transport[] ts = transport_update[transport_key];
        
        uint arrayOwner = ts.length;
        for (uint i=0; i<arrayOwner; i++) {
          Transports(ts[i].transport_key,ts[i].builder_address,ts[i].transport_location,ts[i].latlng,ts[i].transport_status,ts[i].license,ts[i].transport_detail);
            
        }
        return ts;
    }
    
    function getCarHistory(string _car_key)public returns(Car[]) {
        
        bytes32 car_key = stringToBytes32(_car_key);
        
        Car[] car = car_update[car_key];
        
        uint arrayOwner = car.length;
        for (uint i=0; i<arrayOwner; i++) {
            Cars(car[i].car_key,car[i].transport_key,car[i].car_address,car[i].datetime,car[i].status,car[i].car_detail,car[i].transport_detail);
        }
        return car;
    }
    
    function stringToBytes32(string source) returns (bytes32 result) {
        bytes memory tempEmptyStringTest = bytes(source);
        if (tempEmptyStringTest.length == 0) {
            return 0x0;
        }
        assembly {
            result := mload(add(source, 32))
        }
    }
    
}