pragma solidity ^0.4.24;
pragma experimental ABIEncoderV2;

contract AgencyContract {
    
    mapping (address => bool) private _owners;
    
    function OwnedContract(address addr) public returns(bool){
        _owners[addr] = true;
        return true;
    }
    
}