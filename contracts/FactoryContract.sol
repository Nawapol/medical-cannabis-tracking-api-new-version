pragma solidity ^0.4.24;
pragma experimental ABIEncoderV2;

contract FactoryContract {
    
    mapping (address => bool) private _owners;
    
    function OwnedContract(address addr) public returns(bool){
        _owners[addr] = true;
        return true;
    }
    
    struct Factory{
        bytes32 factory_key;
        address builder_address;
        string factory_location;
        bytes32 latlng;
        bytes32 factory_status;
        bytes32 license;
        string factory_detail;
    }
    
    struct Lot{
        bytes32 lot_key;
        bytes32 factory_key;
        address lot_address;
        bytes32 datetime;
        bytes32 status;
        string staff;
        string lot_detail;
    }
    
    event Factorys(
        bytes32 factory_key,
        address builder_address,
        string factory_location,
        bytes32 latlng,
        bytes32 factory_status,
        bytes32 license,
        string factory_detail
    );
    
    event Lots(
        bytes32 lot_key,
        bytes32 factory_key,
        address lot_address,
        bytes32 datetime,
        bytes32 status,
        string staff,
        string lot_detail
    );
    
    mapping (bytes32 => Factory) factorys;
    mapping (bytes32 => Factory[]) factory_update;
    mapping (bytes32 => Lot) lots;
    mapping (bytes32 => Lot[]) lot_update;
    
    function createFactory(string _factory_key, address _builder_address, string _factory_location,string _latlng, string _factory_status, 
    string _license, string _factory_detail)public returns(bool) {
        
        bytes32 factory_key = stringToBytes32(_factory_key);
        bytes32 factory_location = stringToBytes32(_factory_location);
        bytes32 latlng = stringToBytes32(_latlng);
        bytes32 factory_status = stringToBytes32(_factory_status);
        bytes32 license = stringToBytes32(_license);
        bytes32 factory_detail = stringToBytes32(_factory_detail);
        
        factorys[factory_key] = Factory(factory_key, _builder_address, _factory_location, latlng, factory_status, license, _factory_detail);
        factory_update[factory_key].push(Factory(factory_key, _builder_address, _factory_location, latlng, factory_status, license, _factory_detail));
        return true;
        
    }
    
    function createLot(string _lot_key, string _factory_key, address _lot_address, string _datetime, string _status, string _staff,string _lot_detail)public returns(bool) {
        
        bytes32 factory_key = stringToBytes32(_factory_key);
        bytes32 lot_key = stringToBytes32(_lot_key);
        bytes32 datetime = stringToBytes32(_datetime);
        bytes32 status = stringToBytes32(_status);
        
        lots[lot_key] = Lot(lot_key, factory_key, _lot_address, datetime, status, _staff,_lot_detail);
        lot_update[lot_key].push(Lot(lot_key, factory_key, _lot_address, datetime, status, _staff, _lot_detail));
        return true;
        
    }
    
    function getFactory(string _factory_key)public returns(bool) {
        
        bytes32 factory_key = stringToBytes32(_factory_key);
        
        Factory use = factorys[factory_key];
        emit Factorys(use.factory_key, use.builder_address, use.factory_location, use.latlng, use.factory_status, use.license, use.factory_detail);
        
    }
    
    function getLot(string _lot_key)public returns(Lot) {
        
        bytes32 lot_key = stringToBytes32(_lot_key);
        
        Lot use = lots[lot_key];
        emit Lots(use.lot_key, use.factory_key, use.lot_address, use.datetime, use.status, use.staff ,use.lot_detail);
        return use;
        
    }
    
    function getFactoryHistory(string _factory_key)public returns(Factory[]){
        
        bytes32 factory_key = stringToBytes32(_factory_key);
        
        Factory[] fc = factory_update[factory_key];
        
        uint arrayOwner = fc.length;
        for (uint i=0; i<arrayOwner; i++) {
          Factorys(fc[i].factory_key,fc[i].builder_address,fc[i].factory_location,fc[i].latlng,fc[i].factory_status,fc[i].license,fc[i].factory_detail);
            
        }
        return fc;
    }
    
    function getLotHistory(string _lot_key)public returns(Lot[]) {
        
        bytes32 lot_key = stringToBytes32(_lot_key);
        
        Lot[] lot = lot_update[lot_key];
        
        uint arrayOwner = lot.length;
        for(uint i=0; i<arrayOwner; i++) {
            Lots(lot[i].lot_key,lot[i].factory_key,lot[i].lot_address,lot[i].datetime,lot[i].status,lot[i].staff,lot[i].lot_detail);
        }
        return lot;
        
    }
    
    function stringToBytes32(string source) returns (bytes32 result) {
        bytes memory tempEmptyStringTest = bytes(source);
        if (tempEmptyStringTest.length == 0) {
            return 0x0;
        }
        assembly {
            result := mload(add(source, 32))
        }
    }
    
}