pragma solidity ^0.4.24;
pragma experimental ABIEncoderV2;

contract UserContract {
    
    mapping (address => bool) private _owners;
    
    function OwnedContract(address addr) public returns(bool){
        _owners[addr] = true;
        return true;
    }
    
    struct Owner{
        bytes32 owner_key;
        address owner_address;
        bytes32 owner_type;
        bytes32 owner_status;
        address builder_address;
        bytes32 location_key;
        string owner_detail;
    }
    
    event Owners(
        bytes32 owner_key,
        address owner_address,
        bytes32 owner_type,
        bytes32 owner_status,
        address builder_address,
        bytes32 location_key,
        string owner_detail
    );
    
    mapping (bytes32 => Owner) owners;
    mapping (bytes32 => Owner[]) owner_update;
    
    function createOwner(string _owner_key, address _owner_address, string _owner_type, string _owner_status, address _builder_address,  
    string _location_key, string _owner_detail)public returns(bool) {
        
        bytes32 owner_key = stringToBytes32(_owner_key);
        bytes32 owner_type = stringToBytes32(_owner_type);
        bytes32 owner_status = stringToBytes32(_owner_status);
        bytes32 location_key = stringToBytes32(_location_key);
        
        owners[owner_key] = Owner(owner_key, _owner_address, owner_type, owner_status, _builder_address ,location_key, _owner_detail);
        owner_update[owner_key].push(Owner(owner_key,_owner_address,owner_type,owner_status,_builder_address,location_key,_owner_detail));
        return true;
        
    }
    
    function getOwner(string _owner_key)public returns(Owner) {
        
        bytes32 owner_key = stringToBytes32(_owner_key);
        
        Owner memory use = owners[owner_key];
        emit Owners(use.owner_key,use.owner_address,use.owner_type,use.owner_status,use.builder_address,use.location_key,use.owner_detail);
        return use;
    }
    
    function getOwnerHistory(string _owner_key)public returns(Owner[]){
        
        bytes32 owner_key = stringToBytes32(_owner_key);
        
        Owner[] ow = owner_update[owner_key];
        
        uint arrayOwner = ow.length;
        for (uint i=0; i<arrayOwner; i++) {
          Owners(ow[i].owner_key,ow[i].owner_address,ow[i].owner_type,ow[i].owner_status,ow[i].builder_address,ow[i].location_key,ow[i].owner_detail);
            
        }
        return ow;
    }
    
    function stringToBytes32(string source) returns (bytes32 result) {
        bytes memory tempEmptyStringTest = bytes(source);
        if (tempEmptyStringTest.length == 0) {
            return 0x0;
        }
        assembly {
            result := mload(add(source, 32))
        }
    }
    
}