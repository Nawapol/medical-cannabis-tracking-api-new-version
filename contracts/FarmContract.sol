pragma solidity ^0.4.24;
pragma experimental ABIEncoderV2;

contract FarmContract {
    
    mapping (address => bool) private _owners;
    
    function OwnedContract(address addr) public returns(bool){
        _owners[addr] = true;
        return true;
    }
    
    struct Farm{
        bytes32 farm_key;
        address builder_address;
        string farm_location;
        bytes32 latlng;
        bytes32 farm_status;
        bytes32 license;
        string farm_detail;
    }
    
    struct Lot{
        bytes32 lot_key;
        bytes32 farm_key;
        address lot_address;
        bytes32 datetime;
        bytes32 status;
        string staff;
        string lot_detail;
    }
    
    event Farms(
        bytes32 farm_key,
        address builder_address,
        string farm_location,
        bytes32 latlng,
        bytes32 farm_status,
        bytes32 license,
        string farm_detail
    );
    
    event Lots(
        bytes32 lot_key,
        bytes32 farm_key,
        address lot_address,
        bytes32 datetime,
        bytes32 status,
        string staff,
        string lot_detail
    );
    
    mapping (bytes32 => Farm) farms;
    mapping (bytes32 => Farm[]) farm_update;
    mapping (bytes32 => Lot) lots;
    mapping (bytes32 => Lot[]) lot_update;
    
    function createFarm(string _farm_key, address _builder_address, string _farm_location, string _latlng, string _farm_status, 
    string _license, string _farm_detail) public returns(bool) {
        
        bytes32 farm_key = stringToBytes32(_farm_key);
        bytes32 latlng = stringToBytes32(_latlng);
        bytes32 farm_status = stringToBytes32(_farm_status);
        bytes32 license = stringToBytes32(_license);
        
        farms[farm_key] = Farm(farm_key, _builder_address, _farm_location, latlng, farm_status, license, _farm_detail);
        farm_update[farm_key].push(Farm(farm_key, _builder_address, _farm_location, latlng, farm_status, license, _farm_detail));
        return true;
        
    }
    
    function createLot(string _lot_key, string _farm_key, address _lot_address, string _datetime, string _status, string _staff, string _lot_detail)public returns(bool) {
        
        bytes32 farm_key = stringToBytes32(_farm_key);
        bytes32 lot_key = stringToBytes32(_lot_key);
        bytes32 datetime = stringToBytes32(_datetime);
        bytes32 status = stringToBytes32(_status);
        
        lots[lot_key] = Lot(lot_key, farm_key, _lot_address, datetime, status, _staff, _lot_detail);
        lot_update[lot_key].push(Lot(lot_key, farm_key, _lot_address, datetime, status, _staff, _lot_detail));
        return true;
        
    }
    
    function getFarm(string _farm_key)public returns(Farm) {
        
        bytes32 farm_key = stringToBytes32(_farm_key);
        
        Farm use = farms[farm_key];
        emit Farms(use.farm_key, use.builder_address, use.farm_location, use.latlng, use.farm_status, use.license, use.farm_detail);
        return use;
    }
    
    function getLot(string _lot_key)public returns(Lot) {
        
        bytes32 lot_key = stringToBytes32(_lot_key);
        
        Lot use = lots[lot_key];
        emit Lots(use.lot_key, use.farm_key, use.lot_address, use.datetime, use.status, use.staff ,use.lot_detail);
        return use;
        
    }
    
    function getFarmHistory(string _farm_key)public returns(Farm[]){
        
        bytes32 farm_key = stringToBytes32(_farm_key);
        
        Farm[] fr = farm_update[farm_key];
        
        uint arrayOwner = fr.length;
        for (uint i=0; i<arrayOwner; i++) {
          Farms(fr[i].farm_key,fr[i].builder_address,fr[i].farm_location,fr[i].latlng,fr[i].farm_status,fr[i].license,fr[i].farm_detail);
            
        }
        return fr;
    }
    
    function getLotHistory(string _lot_key)public returns(Lot[]) {
        
        bytes32 lot_key = stringToBytes32(_lot_key);
        
        Lot[] lot = lot_update[lot_key];
        
        uint arrayOwner = lot.length;
        for(uint i=0; i<arrayOwner; i++) {
            Lots(lot[i].lot_key,lot[i].farm_key,lot[i].lot_address,lot[i].datetime,lot[i].status,lot[i].staff,lot[i].lot_detail);
        }
        return lot;
        
    }
    
    function stringToBytes32(string source) returns (bytes32 result) {
        bytes memory tempEmptyStringTest = bytes(source);
        if (tempEmptyStringTest.length == 0) {
            return 0x0;
        }
        assembly {
            result := mload(add(source, 32))
        }
    }
    
}