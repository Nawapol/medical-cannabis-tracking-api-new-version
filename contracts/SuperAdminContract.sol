pragma solidity ^0.4.24;
pragma experimental ABIEncoderV2;

import './UserContract.sol';
import './FarmContract.sol';
import './TransportContract.sol';
import './FactoryContract.sol';
import './AgencyContract.sol';
import './DoseProductToken.sol';
import './RawProductToken.sol';

contract SuperAdminContract {
    
    UserContract userCt;
    FarmContract farmCt;
    TransportContract transportCt;
    FactoryContract factoryCt;
    AgencyContract agencyCt;
    RawProductToken rapTkCt;
    DoseProductToken dopTkCt;
    
    constructor(address add_user,address add_farm,address add_transport,address add_factory,address add_agency,address add_raptk,address add_doptk){
        userCt = UserContract(add_user);
        farmCt = FarmContract(add_farm);
        transportCt = TransportContract(add_transport);
        factoryCt = FactoryContract(add_factory);
        agencyCt = AgencyContract(add_agency);
        rapTkCt = RawProductToken(add_raptk);
        dopTkCt = DoseProductToken(add_doptk);
    }
    
    mapping (address => bool) private _owners;
    mapping (address => string) private user_type;
    
    modifier isOwner() {
        require(_owners[msg.sender]);
        _;
    }
    
    function OwnedContract() {
        _owners[msg.sender] = true;
    }
    
    function addOwner(address addr,string type_user)public view returns(bool,bool){
        if(keccak256(type_user) == keccak256("farm_admin")) {
            return (userCt.OwnedContract(addr), farmCt.OwnedContract(addr));
        } else if (keccak256(type_user) == keccak256("transport_admin")) {
            return (userCt.OwnedContract(addr), transportCt.OwnedContract(addr));
        } else if (keccak256(type_user) == keccak256("factory_admin")) {
            return (userCt.OwnedContract(addr), factoryCt.OwnedContract(addr));
        } else if (keccak256(type_user) == keccak256("agency")) {
            return (userCt.OwnedContract(addr), agencyCt.OwnedContract(addr));
        } else {
            _owners[msg.sender] = false;
        }
    }
    
    function removeOwner(string _addr) isOwner {
            _owners[msg.sender] = false;
    }
    
    function stringToBytes32(string source) returns (bytes32 result) {
        bytes memory tempEmptyStringTest = bytes(source);
        if (tempEmptyStringTest.length == 0) {
            return 0x0;
        }
        assembly {
            result := mload(add(source, 32))
        }
    }
    
}

